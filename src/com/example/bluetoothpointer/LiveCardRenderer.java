//package com.example.bluetoothpointer;
//
//import com.google.android.glass.timeline.DirectRenderingCallback;
//
//import android.content.Context;
//import android.graphics.Canvas;
//import android.os.SystemClock;
//import android.view.SurfaceHolder;
//import android.view.View;
//
//public class LiveCardRenderer implements DirectRenderingCallback {
//	final static boolean D = true;
//	final static String TAG = "POINTER";
//	
//	// About 30 FPS.
//	private static final long FRAME_TIME_MILLIS = 33;
//
//	private SurfaceHolder mHolder;
//	private boolean mPaused;
////	private RenderThread mRenderThread;
//
//	private final PointerView mView;
//    private final PointerView.ChangeListener mListener = new PointerView.ChangeListener() {
//        @Override
//        public void onChange() {
//            if (mHolder != null) {
//                draw();
//            }
//        }
//    };
//    public LiveCardRenderer(Context c) {
//    	mView = new PointerView(c);
//    	mView.setListener(mListener);
//    }
//    public LiveCardRenderer(PointerView v) {
//    	mView = v;
//    	mView.setListener(mListener);
//    }
//    
//	@Override
//	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//        // Measure and layout the view with the canvas dimensions.
//        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
//        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
//
//        mView.measure(measuredWidth, measuredHeight);
//        mView.layout(0, 0, mView.getMeasuredWidth(), mView.getMeasuredHeight());
//        draw();
//	}
//
//	@Override
//	public void surfaceCreated(SurfaceHolder holder) {
//        // The creation of a new Surface implicitly resumes the rendering.
//        mRenderingPaused = false;
//        mHolder = holder;
//        draw();
////		updateRendering();
//	}
//
//	@Override
//	public void surfaceDestroyed(SurfaceHolder holder) {
//		mHolder = null;
//		
////		updateRendering();
//	}
//
//	@Override
//	public void renderingPaused(SurfaceHolder holder, boolean paused) {
//		mPaused = paused;
//		draw();
////		updateRendering();
//	}
//
////	/**
////	 * Start or stop rendering according to the timeline state.
////	 */
////	private synchronized void updateRendering() {
////		boolean shouldRender = (mHolder != null) && !mPaused;
////		boolean rendering = mRenderThread != null;
////
////		if (shouldRender != rendering) {
////			if (shouldRender) {
////				mRenderThread = new RenderThread();
////				mRenderThread.start();
////			} else {
////				mRenderThread.quit();
////				mRenderThread = null;
////			}
////		}
////	}
//
//	/**
//	 * Draws the view in the SurfaceHolder's canvas.
//	 */
//	public void draw(View view) {
//		Canvas canvas;
//		try {
//			canvas = mHolder.lockCanvas();
//		} catch (Exception e) {
//			return;
//		}
//		if (canvas != null) {
//			// Draw on the canvas.
//			mHolder.unlockCanvasAndPost(canvas);
//		}
//	}
//	public void draw() {
//		draw(null);
//	}
//
////	/**
////	 * Redraws in the background.
////	 */
////	private class RenderThread extends Thread {
////		private boolean mShouldRun;
//		private boolean mRenderingPaused;
////		
////		/**
////		 * Initializes the background rendering thread.
////		 */
////		public RenderThread() {
////			mShouldRun = true;
////		}
////
////		/**
////		 * Returns true if the rendering thread should continue to run.
////		 *
////		 * @return true if the rendering thread should continue to run
////		 */
////		private synchronized boolean shouldRun() {
////			return mShouldRun;
////		}
////
////		/**
////		 * Requests that the rendering thread exit at the next
////         opportunity.
////		 */
////		public synchronized void quit() {
////			mShouldRun = false;
////		}
////
////		@Override
////		public void run() {
////			while (shouldRun()) {
////				draw();
////				SystemClock.sleep(FRAME_TIME_MILLIS);
////			}
////		}
////
////	}
//}