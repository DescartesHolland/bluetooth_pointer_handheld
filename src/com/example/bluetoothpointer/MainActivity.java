package com.example.bluetoothpointer;

import java.util.logging.LogRecord;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Build;

public class MainActivity extends Activity {
	final static boolean D = true;
	final static String TAG = "POINTER";

	private TextView tv;
	ImageView pointer;
	Pointer pointerObj;

	private Handler mHandler = new UIHandler();
	class UIHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
//			tv.setText("Prac Prac Prac");
			pointer.setX(pointer.getX()+3);
			super.handleMessage(msg);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.card_pointer);
		pointerObj = new Pointer(10, 10);
		if (savedInstanceState == null) {
			//			getFragmentManager().beginTransaction()
			//					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		tv = (TextView) findViewById(R.id.tv);
		pointer = (ImageView) findViewById(R.id.cursor);
		
		if(D) Log.d(TAG, "updating pointer from MA");

		Thread thread = new Thread() {
			public void run() {
				try {
					// simulate long running operation
					Thread.sleep(1500);
					for(int i = 0; i < 30; i++) {
						Thread.sleep(12);
						Message msg = Message.obtain(mHandler);
						msg.obj = "new value";
						mHandler.sendMessage(msg);
					}
				} catch (InterruptedException e) {
					Log.e(TAG, "run in thread", e);
				}
			}
		};
		thread.start();
		//		setContentView(R.layout.card_pointer);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
