package com.example.android.BluetoothChat;

import java.io.Serializable;

import android.graphics.Point;

/**
 * @author Descartes
 *
 */
public class SerializablePoint extends Point implements Serializable {
	
	private static final long serialVersionUID = -8474241235572815379L;
	int x;
	int y;
	
	public SerializablePoint() {
		// TODO Auto-generated constructor stub
		x = 0;
		y = 0;
	}
	
	/**
	 * @param src
	 */
	public SerializablePoint(Point src) {
		super(src);
		x = src.x;
		y = src.y;
	}
	
	/**
	 * @param x
	 * @param y
	 */
	public SerializablePoint(int x, int y) {
		super(x, y);
		this.x = x;
		this.y = y;
	}

	
	/**
	 * @return A new SerializablePoint with each dimension scaled by a factor of 1/2.
	 */
	public SerializablePoint half() {
		return new SerializablePoint((int) (this.x/2.), (int) (this.y/2.));
	}
	
}
