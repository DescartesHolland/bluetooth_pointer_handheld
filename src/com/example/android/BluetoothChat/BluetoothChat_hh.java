
package com.example.android.BluetoothChat;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bluetoothpointer.R;
import com.google.zxing.integration.android.IntentIntegrator;

/**
 * This is the main Activity that displays the current chat session.
 */
public class BluetoothChat_hh extends Activity implements OnDragListener{
	Context con;
	// Debugging
	private static final String TAG = "BT_handheld";
	private static final boolean D = true;

	// Message types sent from the BluetoothChatService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the BluetoothChatService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int DISPLAYED_QR = 15;

	// Layout Views
	private ListView mConversationView;
	private EditText mOutEditText;
	private Button mSendButton;
	private ImageView cursor;
	private RelativeLayout cursor_window;
	private Button qr_button;
	public boolean alreadyConnectedOnce = false;
	static SerializablePoint lastPointSent;

	// Name of the connected device
	private String mConnectedDeviceName = null;
	// Array adapter for the conversation thread
	private ArrayAdapter<String> mConversationArrayAdapter;
	// String buffer for outgoing messages
	private StringBuffer mOutStringBuffer;
	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	// Member object for the chat services
	private BluetoothChatService_hh mChatService = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		con = this;
		if(D) Log.e(TAG, "+++ ON CREATE +++");

		// Set up the window layout
		setContentView(R.layout.main);

		setupBluetooth();

		qr_button = (Button) findViewById(R.id.qr_btn);
		if(qr_button == null)
			Log.d(TAG, "Qr is null");
		qr_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.d(TAG, "in onClick");
				mChatService.QR_RUNNING = true;
				mChatService.start();
				IntentIntegrator ii = new IntentIntegrator((Activity) con);
				AlertDialog ad = (new AlertDialog.Builder(con)).create();
				ad = ii.shareText(mBluetoothAdapter.getAddress());
				//				//alreadyConnectedOnce = true;
			}});

		cursor = (ImageView) findViewById(R.id.cursor_handheld);
		cursor_window = (RelativeLayout) findViewById(R.id.RelativeLayout1);

		cursor_window.setOnDragListener(this);

		cursor.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent me) {
				if(me.getAction() == MotionEvent.ACTION_DOWN) {
					ClipData cursor_location = ClipData.newPlainText("", String.valueOf(cursor.getX()));
					view.startDrag(cursor_location, CustomDragShadowBuilder.fromResource(con, android.R.drawable.ic_input_add), view, 0);
					//				view.setVisibility(View.INVISIBLE);
					return true;
				}
				return false; } } );

	}

	@Override
	public void onStart() {
		super.onStart();
		if(D) Log.e(TAG, "++ ON START hh ++");

		// If BT is not on, request that it be enabled.
		// setupChat() will then be called during onActivityResult
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			// Otherwise, setup the chat session
		} else {
			if (mChatService == null)
				setupChat();
		}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if(D) Log.e(TAG, "+ ON RESUME hh+");

		// Performing this check in onResume() covers the case in which BT was
		// not enabled during onStart(), so we were paused to enable it...
		// onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
		if (mChatService != null) {
			Log.d(TAG, "mChatService null in onResume hh");
			// Only if the state is STATE_NONE, do we know that we haven't started already
			if (mChatService.getState() == BluetoothChatService_hh.STATE_NONE) {
				// Start the Bluetooth chat services
				mChatService.start();
			}
		}
		else Log.d(TAG, "mChatService is null handheld");
	}

	private void setupBluetooth() {
		// Get local Bluetooth adapter
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
			Log.d(TAG, "Finishing!");
			finish();
			return;
		}
	}

	private void setupChat() {
		Log.d(TAG, "setupChat()");

		// Initialize the array adapter for the conversation thread
		mConversationArrayAdapter = new ArrayAdapter<String>(this, R.layout.message);
		mConversationView = (ListView) findViewById(R.id.in);
		mConversationView.setAdapter(mConversationArrayAdapter);

		// Initialize the compose field with a listener for the return key
		mOutEditText = (EditText) findViewById(R.id.edit_text_out);
		mOutEditText.setOnEditorActionListener(mWriteListener);

		// Initialize the send button with a listener that for click events
		mSendButton = (Button) findViewById(R.id.button_send);
		mSendButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Send a message using content of the edit text widget
				TextView view = (TextView) findViewById(R.id.edit_text_out);
				String message = view.getText().toString();
			}
		});

		// Initialize the BluetoothChatService to perform bluetooth connections
		mChatService = new BluetoothChatService_hh(this, mHandler);

		// Initialize the buffer for outgoing messages
		mOutStringBuffer = new StringBuffer("");
	}

	@Override
	public synchronized void onPause() {
		super.onPause();
		if(D) Log.e(TAG, "- ON PAUSE handheld -");
	}

	@Override
	public void onStop() {
		super.onStop();
		if(D) Log.e(TAG, "-- ON STOP --");
		//	    Handler handler = new Handler(new Handler.Callback() {
		//	        @Override
		//	        public boolean handleMessage(Message msg) {
		//	            switch (msg.what) {
		//	                case 1:
		//	            		// Performing this check in onResume() covers the case in which BT was
		//	            		// not enabled during onStart(), so we were paused to enable it...
		//	            		// onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
		//	            		if (mChatService != null) {
		////	            			mChatService.resetConnection();
		//	            			mChatService.connect(mBluetoothAdapter.getRemoteDevice(mBluetoothAdapter.getAddress()));
		//	            			// Only if the state is STATE_NONE, do we know that we haven't started already
		//	            			if (mChatService.getState() == BluetoothChatService_hh.STATE_NONE) {
		//	            				// Start the Bluetooth chat services
		//	            				mChatService.start();
		//	            			}
		//	            		}
		//	                default:
		//	                    break;
		//	            }
		//	            return false;
		//	        }
		//	    });
		//	    handler.sendEmptyMessageDelayed(1, 1000);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth chat services
		if (mChatService != null) mChatService.stop();
		if(D) Log.e(TAG, "--- ON DESTROY ---");
	}

	private void ensureDiscoverable() {
		if(D) Log.d(TAG, "ensure discoverable");
		if (mBluetoothAdapter.getScanMode() !=
				BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	/**
	 * Sends a message.
	 * @param point  A string of text to send.
	 */
	private void sendMessage(SerializablePoint point) {
		// Check that we're actually connected before trying anything
		if (mChatService.getState() != BluetoothChatService_hh.STATE_CONNECTED) {
			Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
			return;
		}

		SerializablePoint sp = new SerializablePoint(point);
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			mChatService.write(sp.half());
		}
		else 
			mChatService.write(sp);

		// Reset out string buffer to zero and clear the edit text field
		mOutStringBuffer.setLength(0);
		mOutEditText.setText(mOutStringBuffer);
	}

	// The action listener for the EditText widget, to listen for the return key
	private TextView.OnEditorActionListener mWriteListener =
			new TextView.OnEditorActionListener() {
		public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
			// If the action is a key-up event on the return key, send the message
			if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
				String message = view.getText().toString();
				SerializablePoint p = new SerializablePoint(0, 0);
				sendMessage(p);
			}
			if(D) Log.i(TAG, "END onEditorAction");
			return true;
		}
	};

	// The Handler that gets information back from the BluetoothChatService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
				switch (msg.arg1) {
				case BluetoothChatService_hh.STATE_CONNECTED:
					mConversationArrayAdapter.clear();
					break;
				case BluetoothChatService_hh.STATE_CONNECTING:
					break;
				case BluetoothChatService_hh.STATE_LISTEN:
					break;
				case BluetoothChatService_hh.STATE_NONE:
					break;
				}
				break;
			case MESSAGE_WRITE:
				byte[] writeBuf = (byte[]) msg.obj;

				// construct a string from the buffer
				String writeMessage = new String(writeBuf);
				mConversationArrayAdapter.add("Me:  " + writeMessage);
				break;
			case MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				// construct a string from the valid bytes in the buffer
				String readMessage = new String(readBuf, 0, msg.arg1);
				if(D) Log.d(TAG, "Read Message: "+readMessage);
				mConversationArrayAdapter.add(mConnectedDeviceName+":  " + readMessage);
				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to "
						+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
				break;
			case MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
						Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.scan:
			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			return true;
		case R.id.discoverable:
			// Ensure this device is discoverable by others
			ensureDiscoverable();
			return true;
		}
		return false;
	}

	public boolean onDrag(View arg0, DragEvent arg1) {
		//		Log.d("HOLA", "Dragging");
		switch(arg1.getAction()) {
		case DragEvent.ACTION_DRAG_STARTED:
			return true;
		case DragEvent.ACTION_DRAG_LOCATION:
			if(lastPointSent == null) {
				lastPointSent = new SerializablePoint((int) (arg1.getX()-cursor.getWidth()/2.)
						, (int) (arg1.getY()-cursor.getHeight()/2.));
				sendMessage(lastPointSent);
			}
			SerializablePoint pointToSend = new SerializablePoint(
					(int) (arg1.getX()-cursor.getWidth()/2.),
					(int) (arg1.getY()-cursor.getHeight()/2.) );

			//Don't send repeat points:
			if(!pointToSend.equals(lastPointSent)) {
				sendMessage(pointToSend);
				lastPointSent = new SerializablePoint(pointToSend);
			}

			//Setting opacity of drag shadow didn't work; remove if performance is an issue
			cursor.setX((float) (arg1.getX()-cursor.getWidth()/2.));//(float) (temp[0]));//+cursor.getWidth()/2.));
			cursor.setY((float) (arg1.getY()-cursor.getHeight()/2.));//(float) (temp[1]));//+cursor.getHeight()/2.));
			break;
		case DragEvent.ACTION_DROP:
			Log.d(TAG, "Dropping at "+arg1.getX()+" "+arg1.getY());

			sendMessage(new SerializablePoint((int) (arg1.getX()-cursor.getWidth()/2.)
					, (int) (arg1.getY()-cursor.getHeight()/2.)));

			cursor.setX((float) (arg1.getX()-cursor.getWidth()/2.));
			cursor.setY((float) (arg1.getY()-cursor.getHeight()/2.));
			break;
		case DragEvent.ACTION_DRAG_ENTERED:
			break;
		case DragEvent.ACTION_DRAG_ENDED:
			break;
		case DragEvent.ACTION_DRAG_EXITED:
			break;
		}
		return false;
	}

	/**
	 * Re-registers UI widgets for when orientation changes.
	 * This prevents the bluetooth listener from being reset in onCreate()
	 */
	private void modifiedOnCreate() {
		qr_button = (Button) findViewById(R.id.qr_btn);
		qr_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				setupBluetooth(); //to prevent disconnects when re-scanning QR
				//Gets device's bluetooth MAC and turning it into an on-screen QR code
				IntentIntegrator ii = new IntentIntegrator((Activity) con);
				AlertDialog ad = ii.shareText(mBluetoothAdapter.getAddress());
			}});

		cursor = (ImageView) findViewById(R.id.cursor_handheld);
		cursor_window = (RelativeLayout) findViewById(R.id.RelativeLayout1);

		cursor_window.setOnDragListener(this);

		cursor.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent me) {
				if(me.getAction() == MotionEvent.ACTION_DOWN) {
					ClipData cursor_location = ClipData.newPlainText("", String.valueOf(cursor.getX()));
					view.startDrag(cursor_location, CustomDragShadowBuilder.fromResource(con, android.R.drawable.ic_input_add), view, 0);
					//				view.setVisibility(View.INVISIBLE);
					return true;
				}
				return false; } } );
	}

	/**
	 * Prevents onCreate() from being called when orientation changes.
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			setContentView(R.layout.main);
			modifiedOnCreate();
		} else {
			setContentView(R.layout.main);
			modifiedOnCreate();
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(D) Log.d(TAG, "onActivityResult " + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK) {

				// Get the device MAC address
				String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				Log.d(TAG, "Device MAC: "+address);

				// Get the BluetoothDevice object
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

				// Attempt to connect to the device
				mChatService.connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK) {
				// Bluetooth is now enabled, so set up a chat session
				setupChat();
			} else {
				// User did not enable Bluetooth or an error occured
				Log.d(TAG, "BT not enabled");
				Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
				finish();
			}
		case DISPLAYED_QR:
			Log.d(TAG, "Setting QR_RUNNING false in handheld");
			mChatService.QR_RUNNING = false;
			break;
		}
	}
}