/**
 * 
 */
package com.example.android.BluetoothChat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.View.DragShadowBuilder;

public class CustomDragShadowBuilder extends DragShadowBuilder {
	private Drawable shadow;
	// Debugging
	private static final String TAG = "BT_handheld";
	private static final boolean D = true;
	
	CustomDragShadowBuilder() {
		super();
	}
	
	public static View.DragShadowBuilder fromResource(Context context, int drawableId) {
		CustomDragShadowBuilder builder = new CustomDragShadowBuilder();
		
		builder.shadow = context.getResources().getDrawable(drawableId);
		if (builder.shadow == null) {
			Log.e(TAG, "Drawable from id is null");
			throw new NullPointerException("Drawable from id is null");
		}
		
		builder.shadow.setBounds(0, 0, builder.shadow.getMinimumWidth(), builder.shadow.getMinimumHeight());
		
		return builder;
	}
	
	public static View.DragShadowBuilder fromBitmap(Context context, Bitmap bmp) {
		if (bmp == null) {
			throw new IllegalArgumentException("Bitmap cannot be null");
		}
		
		CustomDragShadowBuilder builder = new CustomDragShadowBuilder();
		
		builder.shadow = new BitmapDrawable(context.getResources(), bmp);
		builder.shadow.setBounds(0, 0, builder.shadow.getMinimumWidth(), builder.shadow.getMinimumHeight());
		
		return builder;
	}
	
	@Override
	public void onDrawShadow(Canvas canvas) {
		shadow.setAlpha(255);
		shadow.draw(canvas);
	}
	
	@Override
	public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint) {
		shadowSize.x = shadow.getMinimumWidth();
		shadowSize.y = shadow.getMinimumHeight();
		
		shadowTouchPoint.x = (int)(shadowSize.x / 2);
		shadowTouchPoint.y = (int)(shadowSize.y / 2);
	}
}
