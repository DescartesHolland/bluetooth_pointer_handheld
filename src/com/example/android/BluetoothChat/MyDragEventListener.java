package com.example.android.BluetoothChat;

import android.graphics.Point;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;

/**
 * @author hollandd
 *
 */
public class MyDragEventListener implements OnDragListener {

	/* (non-Javadoc)
	 * @see android.view.View.OnDragListener#onDrag(android.view.View, android.view.DragEvent)
	 */
	@Override
	public boolean onDrag(View arg0, DragEvent arg1) {
//		Log.d("HOLA", "Dragging");
		switch(arg1.getAction()) {
		case DragEvent.ACTION_DROP:
			Log.d("HOLA", "Dropping at "+arg1.getX()+" "+arg1.getY());
//			BluetoothChat_hh.sendMsg(new Point(arg1.getX(), arg1.getY()));
			break;
		case DragEvent.ACTION_DRAG_STARTED:
			Log.d("HOLA", "Started");
			return true;
//			break;
		case DragEvent.ACTION_DRAG_ENTERED:
			Log.d("HOLA", "Entered");
			break;
		case DragEvent.ACTION_DRAG_ENDED:
			Log.d("HOLA", "Ended");
			break;
		case DragEvent.ACTION_DRAG_LOCATION:
			Log.d("HOLA", "Location "+arg1.getX()+" "+arg1.getY());
			break;
		case DragEvent.ACTION_DRAG_EXITED:
			Log.d("HOLA", "Exited");
			break;
		}
//		if(arg1.getAction() == DragEvent.ACTION_DRAG_LOCATION) {
//		if(arg1.getAction() == DragEvent.ACTION_DROP) {
//			Log.d("HOLA", "Dropping");
//			arg0.setX(arg1.getX());
//			arg0.setY(arg1.getY()); 
//		}
//		}
		return false;
	}

}
